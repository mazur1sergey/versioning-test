## [3.0.1](https://gitlab.com/mazur1sergey/versioning-test/compare/v3.0.0...v3.0.1) (2022-06-09)


### Bug Fixes

* test ([568336e](https://gitlab.com/mazur1sergey/versioning-test/commit/568336e4177067c4269cbd3daea0ab0eb500e9f2))

# [3.0.0](https://gitlab.com/mazur1sergey/versioning-test/compare/v2.3.0...v3.0.0) (2022-04-21)


### Features

* test ([d053af5](https://gitlab.com/mazur1sergey/versioning-test/commit/d053af5f03e16e85309a6d2cb4f96f3135ef4d7a))


### BREAKING CHANGES

* semantic-release

# [2.3.0](https://gitlab.com/mazur1sergey/versioning-test/compare/v2.2.1...v2.3.0) (2022-04-21)


### Bug Fixes

* fix semantic config ([2359813](https://gitlab.com/mazur1sergey/versioning-test/commit/2359813c1d77a94e64cbab1dfa4586d926e519fb))


### Features

* add npm ([167f0fb](https://gitlab.com/mazur1sergey/versioning-test/commit/167f0fb6bb2a4ffe7c30aa69395ca829fc63bd5a))

## [2.2.1](https://gitlab.com/mazur1sergey/versioning-test/compare/v2.2.0...v2.2.1) (2022-04-21)


### Bug Fixes

* config ([a083433](https://gitlab.com/mazur1sergey/versioning-test/commit/a0834337ea18f81d95314ea2b01817800c1bbe3b))

# [1.1.0](https://gitlab.com/mazur1sergey/versioning-test/compare/v1.2.0...v1.1.0) (2022-04-21)


### Bug Fixes

* gitlab-ci ([06889a1](https://gitlab.com/mazur1sergey/versioning-test/commit/06889a13b970d5995308c3d1d70224ffc534a690))
* gitlab-ci ([261044e](https://gitlab.com/mazur1sergey/versioning-test/commit/261044ec7d72b8e176065f898d5ee1cb5e5093e2))


### Features

* add changelog ([0ad4db0](https://gitlab.com/mazur1sergey/versioning-test/commit/0ad4db0dc11edcf6b6925441bf2ab5cb31a38427))
* add changelog ([8e9b1f2](https://gitlab.com/mazur1sergey/versioning-test/commit/8e9b1f26f83fb6a087eba075bc9beaea5d98d606))
* add changelog configure ([4d9f99b](https://gitlab.com/mazur1sergey/versioning-test/commit/4d9f99b6eefc2bdb6010380f91938d9ba0fbfd5f))
* add semantic-release/git ([e891fbf](https://gitlab.com/mazur1sergey/versioning-test/commit/e891fbf3f7d84ae6934e1bf4669d6b7079cd6ce6))
* change version ([f0dd85b](https://gitlab.com/mazur1sergey/versioning-test/commit/f0dd85b1e9e85084df9e96bc83c6023833a61370))
* first commit ([4412733](https://gitlab.com/mazur1sergey/versioning-test/commit/44127339b76a10c0aae3a6a1ca072f9612aa426e))
* test ([e52c07a](https://gitlab.com/mazur1sergey/versioning-test/commit/e52c07a2abe5d584b5c67bd27cad6e0108dbbde7))
* test ([d1b6fb2](https://gitlab.com/mazur1sergey/versioning-test/commit/d1b6fb25f0bdb8559301c95b4dde34472aa0f889))
* test ([2e601fe](https://gitlab.com/mazur1sergey/versioning-test/commit/2e601fec8a839942f89c22f018256b2681f52017))
* test commit ([4ecb11a](https://gitlab.com/mazur1sergey/versioning-test/commit/4ecb11add7e3b93948f37b8a22025daea40be536))
* test2 ([2a7cd9a](https://gitlab.com/mazur1sergey/versioning-test/commit/2a7cd9a7f7f707f694bf2e1b80cfd8a33a07cdda))
* test222 ([168691f](https://gitlab.com/mazur1sergey/versioning-test/commit/168691f44ed85a91dc520a42fd3cedd1a27f6dbc))


### BREAKING CHANGES

* test222
* test
